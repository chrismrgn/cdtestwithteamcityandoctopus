﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CDTestWithTeamCityAndOctopus.Startup))]
namespace CDTestWithTeamCityAndOctopus
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
